# MEAN Stack (Angular 5) CRUD Web Application Example

This source code is part of [MEAN Stack (Angular 5) CRUD Web Application Example](https://www.djamware.com/post/5a0673c880aca7739224ee21/mean-stack-angular-5-crud-web-application-example)

To run locally:

* Clone this repo
* Run `npm install`
* Run `npm start`

MongoDb installation tutorials for all platforms https://docs.mongodb.com/manual/installation/#mongodb-community-edition

Example run on http://localhost:3000/


![Customer overview](./doc/overviewCustomer.png)
![Customer add form](./doc/addCustomer.png)
![Customer edit form](./doc/editCustomer.png)
![Customer detail overview](./doc/detailCustomer.png)
![Customer gif tour](./doc/customer.gif)
