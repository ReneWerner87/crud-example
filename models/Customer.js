var mongoose = require('mongoose');

var CustomerSchema = new mongoose.Schema({
    title: String,
    firstName: String,
    lastName: String,
    dateOfBirth: String,
    postCode: String,
    city: String,
    street: String,
    streetNumber: String,
    updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Customer', CustomerSchema);
