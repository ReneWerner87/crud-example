import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerDetailComponent implements OnInit {

  customer = {};

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getCustomerDetail(this.route.snapshot.params['id']);
  }

  getCustomerDetail(id) {
    this.http.get('/customer/'+id).subscribe(data => {
      this.customer = data;
    });
  }

  deleteCustomer(id) {
    this.http.delete('/customer/'+id)
      .subscribe(res => {
          this.router.navigate(['/customers']);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
