import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerComponent implements OnInit {

  customers: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('/customer').subscribe(data => {
      console.log(data);
      this.customers = data;
    });
  }

}
