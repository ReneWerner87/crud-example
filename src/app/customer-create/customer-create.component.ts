import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerCreateComponent implements OnInit {

  customer = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  saveCustomer() {
    this.http.post('/customer', this.customer)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/customer-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
