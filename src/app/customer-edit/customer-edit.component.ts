import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerEditComponent implements OnInit {

  customer: any = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCustomer(this.route.snapshot.params['id']);
  }

  getCustomer(id) {
    this.http.get('/customer/'+id).subscribe(data => {
      this.customer = data;
    });
  }

  updateCustomer(id) {
    this.customer.updated_date = Date.now();
    this.http.put('/customer/'+id, this.customer)
      .subscribe(res => {
          let id = res['_id'];
          this.router.navigate(['/customer-details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
